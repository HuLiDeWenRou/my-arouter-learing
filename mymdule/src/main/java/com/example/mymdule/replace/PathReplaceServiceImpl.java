package com.example.mymdule.replace;

import android.content.Context;
import android.net.Uri;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.service.PathReplaceService;

/**
 * @author tgw
 * @date 2021/1/18
 *
 * 改写跳转地址
 */
public class PathReplaceServiceImpl implements PathReplaceService {

    @Override
    public String forString(String path) {
        //自定义逻辑返回路径；

        return null;
    }

    @Override
    public Uri forUri(Uri uri) {
        //自定义逻辑
        return null;
    }

    @Override
    public void init(Context context) {

    }
}

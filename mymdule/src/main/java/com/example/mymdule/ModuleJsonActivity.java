package com.example.mymdule;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.service.SerializationService;
import com.alibaba.android.arouter.launcher.ARouter;
import com.example.mymdule.bean.JsonBean;
import com.example.mymdule.databinding.ModuleActivityMainBinding;

@Route(path = "/module/json/ModuleJsonActivity")
public class ModuleJsonActivity extends AppCompatActivity {
    private ModuleActivityMainBinding mBinding;


    @Autowired(name = "jsonBean")
    public JsonBean jsonBean;//参数必须为公共

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.module_activity_main);

        //方法一
//        ARouter.getInstance().inject(this);//带参数值，接收
//        mBinding.tvContent.setText(jsonBean.toString());

        //方法二
//        SerializationService serializationService = ARouter.getInstance().navigation(SerializationService.class); //通过类名 关联
        //通过注解地址关联
        SerializationService serializationService = (SerializationService) ARouter.getInstance().build("/service/json").navigation();
        serializationService.init(this);
        JsonBean obj = serializationService.parseObject(getIntent().getStringExtra("jsonBean"), JsonBean.class);
        mBinding.tvContent.setText(obj.toString());

    }
}

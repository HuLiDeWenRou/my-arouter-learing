package com.example.mymdule.pretreatment;

import android.content.Context;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.service.PretreatmentService;

/**
 * @author tgw
 * @date 2021/1/18
 *
 * 必须要设置路由 path  否则不会调用该类
 */
@Route(path = "/pretreatment/service")
public class PretreatmentServiceImpl implements PretreatmentService {
    @Override
    public boolean onPretreatment(Context context, Postcard postcard) {
        // Do something before the navigation, if you need to handle the navigation yourself, the method returns false
        //返回false 代表拦截事件
        return true;
    }

    @Override
    public void init(Context context) {

    }
}
package com.example.mymdule;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.example.mymdule.bean.JsonBean;
import com.example.mymdule.databinding.ModuleActivityMainBinding;

@Route(path = "/module/ModuleMainActivity")
public class ModuleMainActivity extends AppCompatActivity {
    private ModuleActivityMainBinding mBinding;

    @Autowired(name = "content")
    public String content;//参数必须为公共


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.module_activity_main);

        ARouter.getInstance().inject(this);//带参数值，接收

        mBinding.tvContent.setText(content);
        mBinding.tvContent.setText(new Intent().getStringExtra("content"));

        findViewById(R.id.bt_to_app_module).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(ModuleMainActivity.this,MainActivity.class));
                //路由实现跨模块
                ARouter.getInstance().build("/main/MainActivity").navigation();
            }
        });
    }
}

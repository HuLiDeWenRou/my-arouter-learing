package com.example.mymdule.interceptor;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Interceptor;
import com.alibaba.android.arouter.facade.callback.InterceptorCallback;
import com.alibaba.android.arouter.facade.template.IInterceptor;
import com.alibaba.android.arouter.launcher.ARouter;

/**
 * @author tgw
 * @date 2021/1/14
 */
// 比较经典的应用就是在跳转过程中处理登陆事件，这样就不需要在目标页重复做登陆检查
// 拦截器会在跳转之间执行，多个拦截器会按  优先级 顺序依次执行
//    priority 优先级使 用Interceptor类注解的priority数值越小，越先执行，优先级越高。
@Interceptor(name = "ceshi", priority = 1)
public class TestInterceptor implements IInterceptor {
    private Context mContext;

    @Override
    public void process(Postcard postcard, InterceptorCallback callback) {
        String path = postcard.getPath();
        Log.d("tgw-ARouter-process", "拦截器");
        String param = postcard.getExtras().getString("content");//获取传递的参数，可从其他地方动态取值

        if (TextUtils.equals(param, "拦截器")) {
            //传递的参数值为 ”拦截器“ 直接跳转
            callback.onContinue(postcard);
        } else if (TextUtils.equals(path, "/module/ModuleInterceptorActivity")){ //如果是跳转 拦截器界面-并且值不为”拦截器“，那就拦截
            //否则拦截，在处理器回调里面再次赋值 为 ”拦截器“
            callback.onInterrupt(new RuntimeException("我觉得有点异常"));
        }else {
            callback.onContinue(postcard);
        }

    }

    @Override
    public void init(Context context) {
        // 拦截器的初始化，会在sdk初始化的时候调用该方法，仅会调用一次
        mContext = context;
        Log.d("tgw-ARouter-process", "拦截器的初始化仅会调用一次");
    }
}

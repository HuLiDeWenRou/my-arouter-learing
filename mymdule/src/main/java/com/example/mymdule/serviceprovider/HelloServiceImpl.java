package com.example.mymdule.serviceprovider;

import android.content.Context;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;

/**
 * @author tgw
 * @date 2021/1/15
 */

// 实现接口
@Route(path = "/service/hello", name = "测试服务")
public class HelloServiceImpl implements HelloService {

    @Override
    public String sayHello(String name) {
        ARouter.getInstance()
                .build("/module/ModuleServiceActivity")
                .withString("content", name)
                .navigation();
        return "通过依赖注入解耦:服务管理，暴露服务, " + name;
    }

    @Override
    public void init(Context context) {

    }
}

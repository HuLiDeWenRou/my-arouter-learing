package com.example.mymdule.serviceprovider;

import android.content.Context;

import com.alibaba.android.arouter.facade.template.IProvider;

/**
 * @author tgw
 * @date 2021/1/15
 */
public interface HelloService extends IProvider {
    String sayHello(String name);
}

package com.example.mymdule.lowlevel;

import android.content.Context;
import android.text.TextUtils;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.service.DegradeService;
import com.alibaba.android.arouter.launcher.ARouter;

/**
 * @author tgw
 * @date 2021/1/15
 *
 * 全局降级
 */
@Route(path = "/degrade/Service")
public class DegradeServiceImpl implements DegradeService {

    @Override
    public void onLost(Context context, Postcard postcard) {
        String path = postcard.getPath();
        if (TextUtils.equals(path, "/module/ModuleOnLostActivity/error")){ //如果是跳转 拦截器界面-并且值不为”拦截器“，那就拦截
            //否则拦截，在处理器回调里面再次赋值 为 ”拦截器“
            ARouter.getInstance()
                    .build("/module/ModuleOnLostActivity")
                    .withString("content", "降级策略DegradeServiceImpl接口中重新寻址")
                    .navigation();
        }

    }

    @Override
    public void init(Context context) {

    }
}

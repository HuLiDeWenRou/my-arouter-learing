package com.example.mymdule;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.example.mymdule.databinding.ModuleActivityMainBinding;

@Route(path = "/module/ModuleServiceActivity")
public class ModuleServiceActivity extends AppCompatActivity {
    private ModuleActivityMainBinding mBinding;

    @Autowired(name = "content")
    public String content;//参数必须为公共

    @Autowired(name = "/service/hello")
    public String contentService;//参数必须为公共


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.module_activity_main);

        ARouter.getInstance().inject(this);//带参数值，接收


        mBinding.tvContent.setText(content+contentService);
    }
}

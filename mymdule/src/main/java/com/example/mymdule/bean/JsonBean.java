package com.example.mymdule.bean;

/**
 * @author tgw
 * @date 2021/1/14
 */
public class JsonBean {
    private String date;
    private String content;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "JsonBean{" +
                "date='" + date + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}

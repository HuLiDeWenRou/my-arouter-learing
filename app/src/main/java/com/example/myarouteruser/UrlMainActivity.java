package com.example.myarouteruser;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.example.myarouteruser.databinding.ActivityMainBinding;
import com.example.myarouteruser.databinding.ActivityMainUrlBinding;
import com.example.mymdule.bean.JsonBean;

import java.util.Random;

@Route(path = "/uri/UrlMainActivity")
public class UrlMainActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityMainUrlBinding mBinding;
    @Autowired(name = "content")
    public JsonBean content;//参数必须为公共

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main_url);
        mBinding.btUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btStartUrl();
            }
        });


        ARouter.getInstance().inject(this);//带参数值，接收
        if (content != null) {
            mBinding.tvContent.setText(content.toString());
        }
    }

    @Override
    public void onClick(View v) {


    }

    //uri跳转不知道意义何在--可用于比如 扫码得到了uri地址然后打开app

    /**
     * Url跳转
     * 网页url跳转Activity
     * 1、网页Url跳转Activity是什么？
     * <p>
     * 效果：点击一个web页面上面的链接，就能跳转到我们的APP页面。
     * 2、网页url跳转Activity的思路
     * <p>
     * 创建中转Activity(SchemeFilterActivity)
     * AndroidManifest中对中转Activity进行设置
     * 在中转Activity中进行对应页面的跳转。
     */
    private void btStartUrl() {
        Uri uri = getIntent().getData();
        //完整包名称,或者下面那个也行
        uri = Uri.parse("tgw://www.tgw.com:1126/uri/UrlMainActivity");//相当于 在配置文件中重新定义了一样
//        uri=  Uri.parse("/uri/UrlMainActivity");
        JsonBean jsonBean = new JsonBean();
        jsonBean.setContent("普通带参数随机值" + new Random().nextInt(100));
        jsonBean.setDate(System.currentTimeMillis() + "");

        ARouter.getInstance().build(uri).withObject("content", jsonBean).navigation();
//        ARouter.getInstance().build(uri).navigation();
    }


    /**  Url跳转 ，参考链接：https://blog.csdn.net/feather_wch/article/details/81605300
     *
     网页url跳转Activity
     1、网页Url跳转Activity是什么？

     效果：点击一个web页面上面的链接，就能跳转到我们的APP页面。
     2、网页url跳转Activity的思路

     创建中转Activity(SchemeFilterActivity)
     AndroidManifest中对中转Activity进行设置
     在中转Activity中进行对应页面的跳转。
     3、功能实现

     1-中转Activity(SchemeFilterActivity.java)

     public class SchemeFilterActivity extends AppCompatActivity{
    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // 1. 获取到Uri
    Uri uri = getIntent().getData();
    // 2. 进行跳转
    ARouter.getInstance()
    .build(uri)
    .navigation();
    // 3.销毁该Activity
    finish();
    }
    }


     2-AndroidManifest.xml对SchemeFilterActivity进行配置

     <activity android:name=".SchemeFilterActivity">
     <intent-filter>
     <data
     android:host="host"
     android:scheme="scheme"/>
     <action android:name="android.intent.action.VIEW"/>
     <category android:name="android.intent.category.DEFAULT"/>
     <category android:name="android.intent.category.BROWSABLE"/>
     </intent-filter>
     </activity>


     3-目标页面

     @Route(path = ARouterConstants.ACTIVITY_URL_LOGIN)
     public class LoginActivity{
     xxx
     }
     -测试的web页面，将该html文件在手机浏览器中打开, 并且点击跳转链接。

     <!DOCTYPE html>
     <html>
     <head>
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <title></title>
     </head>

     <body>
     <h2>跳转测试</h2>
     <h2>自定义Scheme[通常来说都是这样的]</h2>
     <p><a href="scheme://host/app/LoginActivity">scheme://host/app/LoginActivity</a></p>

     </body>
     </html>


     网页url跳转-携带常用类型参数
     在Html页面的跳转中给Url拼接参数

     <p><a href="scheme://host/app/LoginActivity?name=feather&age=18&male=true">scheme://host/app/LoginActivity?name=feather&age=18&male=true</a></p>
     1
     目标Activity接收参数。

     @Autowired(name = "name")
     public String mName;
     @Autowired(name = "age")
     public int mAge;
     @Autowired(name = "male")
     public boolean mIsMale;

     @Override protected void onCreate(Bundle savedInstanceState) {
     super.onCreate(savedInstanceState);

     ARouter.getInstance().inject(this);
     //xxx
     }
     1

     网页url跳转-传递json自动转为自定义对象
     跳转拦截
     报错/处理
     ARouter::There is no route match the path
     解决办法:

     不同module的一级路径必须不同，否则会导致一个moudle中的一级路径失效.
     原因:
     Aouter通过一级目录"app"找到了route，并且在groupIndex中删除了这个路径，代表已经加载到了内存。不同的module使用了相同的一级路径，在Arouter第一次寻找到route的时候便删除了这个一级路径的group，因为一级路径的重复，再调用另一个module的一级路径是"app"的路由时，由于之前Warehouse.groupsIndex已经删除，便导致了there’s no route matched的错误。
     总结
     1、href跳转到app的流程

     系统浏览器发现要加载一个连接，是以arouter开头的协议，但是其自身无法处理该协议，此时就会把此协议往系统层抛
     系统会根据所有安装的app的清单文件决定要将此协议交给哪个app来处理。
     如果要在浏览器上访问，那么浏览器就要自己处理此协议，否在就会交给某个具体app处理
     2、ARouter解决了WebView中URL跳转问题

     如果在WebView中需要跳转到一个Android页面，需要在shouldOverrideUrlLoading中拦截。但是业务的增多，会导致逻辑比较臃肿。
     如果要从其他App跳转到自己APP的某个页面，webView的方法也无法实现了。
     ARouter通过在一个中转Activity统一调度，能简单安全的实现这种需求。
     3、ARouter如何实现重定向？

     实现PathReplaceService接口
     对URL按照一定规则进行处理。*/
}

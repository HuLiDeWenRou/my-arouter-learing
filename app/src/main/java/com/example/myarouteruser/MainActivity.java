package com.example.myarouteruser;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.databinding.DataBindingUtil;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.callback.NavigationCallback;
import com.alibaba.android.arouter.launcher.ARouter;
import com.example.myarouteruser.databinding.ActivityMainBinding;
import com.example.mymdule.bean.JsonBean;
import com.example.mymdule.serviceprovider.HelloService;
import com.example.mymdule.serviceprovider.HelloServiceImpl;

import java.util.Random;

@Route(path = "/main/MainActivity")
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityMainBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mBinding.bt1.setOnClickListener(this);
        mBinding.bt2.setOnClickListener(this);
        mBinding.bt3.setOnClickListener(this);
        mBinding.bt4.setOnClickListener(this);
        mBinding.bt5.setOnClickListener(this);
        mBinding.bt6.setOnClickListener(this);
        mBinding.bt7.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if (viewId == R.id.bt1) {
            btStart1();//普通跳转
        }
        if (viewId == R.id.bt2) {
            btStart2();//普通带参跳转
        }
        if (viewId == R.id.bt3) {
            btStart3();//普通json参跳转
        }
        if (viewId == R.id.bt4) {
            btStart4();//uri跳转
        }
        if (viewId == R.id.bt5) {
            btStart5();//跳转结果监听
        }

        if (viewId == R.id.bt6) {//加动画跳转
            btStart6(v);//降级策略---注意：bt5中的-跳转结果监听的LoginNavigationCallbackImpl 中的onLost 也相当于降级

        }
        if (viewId == R.id.bt7) {//通过依赖注入解耦:服务管理，暴露服务
            btStart7();

        }


    }


    private void btStart1() {
        ARouter.getInstance().build("/module/ModuleMainActivity").navigation();
    }

    private void btStart2() {
        ARouter.getInstance().build("/module/ModuleMainActivity").withString("content", "普通带参数随机值" + new Random().nextInt(100)).navigation();
    }

    private void btStart3() {
        JsonBean jsonBean = new JsonBean();
        jsonBean.setContent("普通带参数随机值" + new Random().nextInt(100));
        jsonBean.setDate(System.currentTimeMillis() + "");
        ARouter.getInstance().build("/module/json/ModuleJsonActivity").withObject("jsonBean", jsonBean).navigation();
    }

    private void btStart4() {
        ARouter.getInstance().build("/uri/UrlMainActivity").navigation();
    }

    private void btStart5() {
        Log.d("tgw-btStart5", "拦截");
        ARouter.getInstance()
                .build("/module/ModuleInterceptorActivity")
                .withString("content", "拦截器值不对")
                .navigation(this, new LoginNavigationCallbackImpl());
    }

    private void btStart6(View v) {
        if (Build.VERSION.SDK_INT >= 16) {
            ActivityOptionsCompat compat = ActivityOptionsCompat.
                    makeScaleUpAnimation(v, v.getWidth() / 2, v.getHeight() / 2, 0, 0);

            ARouter.getInstance()
                    .build("/module/ModuleOnLostActivity/error")
                    .withString("content", "降级策略")
                    .withOptionsCompat(compat)
                    .navigation();
        }

    }

    private void btStart7() {
//        HelloService  helloService = (HelloService) ARouter.getInstance().build("/service/hello").navigation();
        HelloService  helloService = (HelloService) ARouter.getInstance().navigation(HelloService.class);
        helloService.sayHello("通过依赖注入解耦:服务管理，暴露服务-传了个参数");

    }


    public class LoginNavigationCallbackImpl implements NavigationCallback {
        @Override //找到了
        public void onFound(Postcard postcard) {
            Log.d("tgw-onFound", "拦截找到了界面地址");
        }

        @Override //找不到了
        public void onLost(Postcard postcard) {
            Log.d("tgw-onLost", "找不到了界面地址");

        }

        @Override    //跳转成功了
        public void onArrival(Postcard postcard) {
            Log.d("tgw-onArrival", "跳转成功了拦截找到了界面地址");

        }

        @Override
        public void onInterrupt(Postcard postcard) {
            Log.d("tgw-onInterrupt", "拦截了地址");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //此时已在主线程中，可以更新UI了
                    Toast.makeText(MainActivity.this, "参数不对被拦截了,下面将正确值赋值", Toast.LENGTH_LONG).show();
                }
            });
            String path = postcard.getPath(); //还可以根据路径 在拦截器里面做处理
            Bundle bundle = postcard.getExtras();
            bundle.getString("content");
            // 拦截了
            ARouter.getInstance().build("/module/ModuleInterceptorActivity")
                    .with(bundle)
                    .withString("content", "拦截器")
                    .navigation();
        }
    }

}
